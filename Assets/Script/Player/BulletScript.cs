﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public int damage, speed;

    void Awake()
    {
        
    }

    void Update()
    {
        transform.Translate(Vector3.right * Time.deltaTime * speed);
        
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            FindObjectOfType<EnemyLife>().Damage(damage);
            gameObject.SetActive(false);
        }
        
        if (other.gameObject.CompareTag("Boundary"))
        {
            Debug.Log("dsf");
            gameObject.SetActive(false);
        }
    }

}
