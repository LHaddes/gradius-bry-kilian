﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngleBulletScript : MonoBehaviour
{
    public int damage, speed;
    
    void Update()
    {
        transform.Translate((Vector3.up + Vector3.right) * Time.deltaTime * speed);

        
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("oiksndf");
            FindObjectOfType<EnemyLife>().Damage(damage);
            gameObject.SetActive(false);
        }
        
        if (other.gameObject.CompareTag("Boundary"))
        {
            gameObject.SetActive(false);
        }
    }
}
