﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Transform bulletSpawn;

    public bool isShooting;
    public BonusManager bonusManager;

    public BoolVariable doubleShoot, laser, missile;

    private void Awake()
    {
        bonusManager = FindObjectOfType<BonusManager>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            isShooting = true;
            if (!laser.value)
            {
                GameObject bullet = BulletPooler.Instance.SpawnFromPool("Bullet", bulletSpawn);
                if (bullet != null)
                {
                    bullet.SetActive(true);

                    if (!bullet.GetComponent<SpriteRenderer>().isVisible)
                    {
                        bullet.transform.position = bulletSpawn.position;
                    }
                }

                if (doubleShoot.value)
                {
                    GameObject angleBullet = BulletPooler.Instance.SpawnFromPool("AngleBullet", bulletSpawn);
                    if (angleBullet != null)
                    {
                        angleBullet.SetActive(true);
                    }
                    
                    if (!angleBullet.GetComponent<SpriteRenderer>().isVisible)
                    {
                        angleBullet.transform.position = bulletSpawn.position;
                    }
                }
            }
            else
            {
                GameObject bullet = BulletPooler.Instance.SpawnFromPool("Laser", bulletSpawn);
                if (bullet != null)
                {
                    bullet.transform.position = transform.position;
                    bullet.SetActive(true);
                }
                
                if (!bullet.GetComponent<SpriteRenderer>().isVisible)
                {
                    bullet.transform.position = bulletSpawn.position;
                }
            }

            if (missile.value)
            {
                GameObject missile = BulletPooler.Instance.SpawnFromPool("Missile", bulletSpawn);
                if (missile != null)
                {
                    missile.SetActive(true);
                }
                
                if (!missile.GetComponent<SpriteRenderer>().isVisible)
                {
                    missile.transform.position = bulletSpawn.position;
                }
            }
        }
        else
        {
            isShooting = false;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            FindObjectOfType<BonusManager>().BonusUse();
        }
    }
}
