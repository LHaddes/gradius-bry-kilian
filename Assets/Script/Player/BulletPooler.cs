﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPooler : MonoBehaviour
{
    public static BulletPooler Instance;
    public List<GameObject> pooledObjects;
    public List<ObjectPoolItem> itemsToPool;
    
    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } 
        
        else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    void Start () {
        pooledObjects = new List<GameObject>();
        foreach (ObjectPoolItem item in itemsToPool) {
            for (int i = 0; i < item.number; i++) {
                GameObject obj = Instantiate(item.bullet);
                obj.SetActive(false);
                pooledObjects.Add(obj);
            }
        }
    }
    
    public GameObject SpawnFromPool(string tag, Transform position) {
        
        for (int i = 0; i < pooledObjects.Count; i++) 
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].CompareTag(tag)) 
            {
                return pooledObjects[i];
            }
        }
        
        
        foreach (ObjectPoolItem item in itemsToPool) {
            
            if (item.bullet.CompareTag(tag)) {
                
                if (item.hasToExpand) {
                    GameObject obj = Instantiate(item.bullet);
                    obj.SetActive(false);
                    pooledObjects.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }

    public void IncreasePool(string tag)
    {
        foreach (ObjectPoolItem item in itemsToPool) {
            if (item.bullet.CompareTag(tag))
            {
                GameObject obj = Instantiate(item.bullet);
                obj.SetActive(false);
                pooledObjects.Add(obj);
            }
        }
    }
    
    [Serializable]    //On crée une classe pour le nombre d'objet à pull, ainsi que l'objet a pull
    public class ObjectPoolItem {
        public int number;
        public GameObject bullet;
        public bool hasToExpand;
    }
}