﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarunDeplacement : MonoBehaviour
{
    public float speed, magnitude, frequency;
    private Vector3 pos;

    private void Start()
    {
        pos = transform.position;
    }


    // Update is called once per frame
    void Update()
    {
        pos -= transform.right * Time.deltaTime * speed;
        transform.position = pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
    }
}
