﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RugalIA : MonoBehaviour
{
    public float speed, leftSpeed;
    private bool _aligned = false;
    private GameObject player;

    void Awake()
    {
        player = FindObjectOfType<PlayerController>().gameObject;
    }
    
    void Update()
    {
        //Si l'ennemi n'est pas dans l'encadrement de la position y du player, il se déplace en diagonale
        if (!_aligned)
        {
            if ((player.transform.position.y - 0.3) > transform.position.y)
            {
                transform.Translate((Vector3.left + Vector3.up) * speed * Time.deltaTime);
            } 
            else if ((player.transform.position.y + 0.3) < transform.position.y)
            {
                transform.Translate((Vector3.left + Vector3.down) * speed * Time.deltaTime);
            }
            else
            {
                _aligned = true;
            }
        }
        //sinon, il se déplace vers la gauche
        else
        {
            transform.Translate(leftSpeed * Vector3.left * Time.deltaTime);
        }
        
        
    }
}
