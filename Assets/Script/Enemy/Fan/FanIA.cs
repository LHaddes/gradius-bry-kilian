﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanIA : MonoBehaviour
{
    public float speed, positionXToTurn;
    private GameObject player;

    private bool _avant, _diagonale, _arriere, _isAbove;
    
    void Start()
    {
        player = FindObjectOfType<PlayerController>().gameObject;
        _avant = true;
    }

    void Update()
    {
        if (_avant)
        {
            if (transform.position.x > positionXToTurn)
            {
                transform.Translate(speed * Vector3.left * Time.deltaTime);
            }
            else
            {
                _isAbove = transform.position.y < player.transform.position.y;
                _avant = false;
                _diagonale = true;
            }
        } 
        
        else if (_diagonale)
        {
            if (_isAbove)
            {
                transform.Translate( (Vector3.up + Vector3.right) * speed * Time.deltaTime);
                if (transform.position.y > player.transform.position.y)
                {
                    _diagonale = false;
                    _arriere = true;
                }
            }
            else
            {
                transform.Translate((Vector3.down + Vector3.right) * speed * Time.deltaTime);
                if (transform.position.y < player.transform.position.y)
                {
                    _diagonale = false;
                    _arriere = true;
                }
            }
        } 
        
        else if (_arriere)
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
    }
}
