﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FanGroup : MonoBehaviour
{
    public GameObject bonus;
    private bool allDead;

    public Vector3 bonusPos;


    void Update()
    {
        bonusPos = GetComponentInChildren<Transform>().GetChild(3).position;
        if (transform.childCount == 0 && !allDead)
        {
            allDead = true;
            Instantiate(bonus, bonusPos, Quaternion.identity);
        }
    }
}
