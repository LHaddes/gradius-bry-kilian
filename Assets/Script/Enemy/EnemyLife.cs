﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EnemyLife : MonoBehaviour
{

    public int life, maxLife;


    void Start()
    {
        life = maxLife;
    }

    void Update()
    {
        if (life <= 0)
        {
            Death();
        }
    }

    public void Damage(int degat)
    {
        life -= degat;
    }

    void Death()
    {
        Destroy(gameObject);
    }
}
