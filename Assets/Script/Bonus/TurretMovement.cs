﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretMovement : MonoBehaviour
{
    private Transform _playerTransform;

    private Vector3 _lookDirection;
    public float speed;
    public float distanceMin;
    
    void Awake()
    {
        _playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();    //On récupère les infos du Transform du Player
    }
    
    void Update()
    {
        //Permet à la tourelle de suivre le player en restant à une certaine distance
        
        _lookDirection = (_playerTransform.position - transform.position).normalized;
        
        if ((transform.position - _playerTransform.position).magnitude > distanceMin)
        {
            transform.Translate(_lookDirection.x * speed * Time.deltaTime,_lookDirection.y * speed * Time.deltaTime, 0);
        }
    }
}
