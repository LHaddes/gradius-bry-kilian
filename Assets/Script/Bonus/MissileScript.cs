﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileScript : MonoBehaviour
{
    public int damage, speed;
    public Sprite missileOnGround;

    void Awake()
    {
        
    }

    void Update()
    {
        transform.Translate((Vector3.right + Vector3.down) * Time.deltaTime * speed);
        
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("oiksndf");
            FindObjectOfType<EnemyLife>().Damage(damage);

            gameObject.SetActive(false);
        }
        
        if (other.gameObject.CompareTag("Boundary"))
        {
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            GetComponent<SpriteRenderer>().sprite = missileOnGround;
            transform.Translate(Vector3.right * Time.deltaTime * speed);
        }
    } 
}
