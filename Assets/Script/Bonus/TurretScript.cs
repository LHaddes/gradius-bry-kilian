﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : MonoBehaviour
{
    public Transform bulletSpawn;
    
    private PlayerController _playerController;
    private BonusManager _bonusManager;

    public BoolVariable doubleShoot, laser;

    public float fireRate;
    // Start is called before the first frame update
    void Start()
    {
        _playerController = FindObjectOfType<PlayerController>();
        _bonusManager = FindObjectOfType<BonusManager>();
    }

    // Update is called once per frame
    void Update()
    {
        fireRate += Time.deltaTime;
        
        
        if (_playerController.isShooting)
        {
            if (!doubleShoot.value && !laser.value)
            {
                if (fireRate >= 0.3f)
                {
                    BulletPooler.Instance.SpawnFromPool("Bullet", bulletSpawn);
                    fireRate = 0;
                }
            }
            else if (doubleShoot.value)
            {
                if (fireRate >= 0.3f)
                {
                    BulletPooler.Instance.SpawnFromPool("Bullet", bulletSpawn);
                    BulletPooler.Instance.SpawnFromPool("AngleBullet", bulletSpawn);
                    fireRate = 0;
                }
            }
            else if (laser.value)
            {
                if (fireRate >= 0.2f)
                {
                    Debug.Log("piou");
                    BulletPooler.Instance.SpawnFromPool("Laser", bulletSpawn);
                    fireRate = 0;
                }
            }
        }
    }
}
