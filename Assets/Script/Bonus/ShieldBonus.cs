﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBonus : MonoBehaviour
{
    public BoolVariable shield;
    public int hitCounter;

    public void Update()
    {
        if (hitCounter == 5)
        {
            shield.value = false;
            hitCounter = 0;
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("EnemyBullet"))
        {
            hitCounter++;
        }
    }
}
