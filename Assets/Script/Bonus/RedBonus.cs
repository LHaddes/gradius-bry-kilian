﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedBonus : MonoBehaviour
{
    private BonusManager bonusManager;
    // Start is called before the first frame update
    void Awake()
    {
        bonusManager = FindObjectOfType<BonusManager>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            bonusManager.counter.value++;
            Destroy(this.gameObject);
        }
    }
}
