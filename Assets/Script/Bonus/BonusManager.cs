﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BonusManager : MonoBehaviour
{
    public BoolVariable doubleShoot, laser, turret1, turret2, missile, shield;
    public IntVariable counter;

    public PlayerMovement playerMovement;
    public PlayerController playerController;

    public GameObject shieldObject, turretObject1, turretObject2;
    void Awake()
    {
        counter.value = 0;
        
        doubleShoot.value = false;
        laser.value = false;
        turret1.value = false;
        turret2.value = false;
        missile.value = false;
        shield.value = false;

        playerMovement = FindObjectOfType<PlayerMovement>();
        playerController = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        if (counter.value > 7)
        {
            counter.value = 1;
        }
    }

    public void BonusUse()
    {
        switch (counter.value)
        {
            case 1 :
                Debug.Log("Speed");
                playerMovement.speed += 0.5f;
                counter.value = 0;
                break;
                
            case 2 :
                Debug.Log("Missile");
                missile.value = true;
                counter.value = 0;
                break;
                
            case 3:
                Debug.Log("DoubleShot");
                doubleShoot.value = true;
                if (laser.value)
                {
                    laser.value = false;
                }
                
                counter.value = 0;
                break;
                
            case 4 :
                Debug.Log("Laser");
                laser.value = true;
                if (doubleShoot.value)
                {
                    doubleShoot.value = false;
                }
                counter.value = 0;
                break;
                
            case 5 :
                Debug.Log("Tourelle");
                
                if (turret1.value)
                {
                    turret2.value = true;
                    turretObject2.SetActive(true);
                }
                else
                {
                    turret1.value = true;
                    turretObject1.SetActive(true);
                }
                
                BulletPooler.Instance.IncreasePool("Bullet");
                BulletPooler.Instance.IncreasePool("Bullet");
                BulletPooler.Instance.IncreasePool("Laser");
                BulletPooler.Instance.IncreasePool("Laser");
                BulletPooler.Instance.IncreasePool("AngleBullet");
                BulletPooler.Instance.IncreasePool("Missile");
                
                counter.value = 0;
                break;
                
            case 6:
                Debug.Log("Shield");
                shield.value = true;
                shieldObject.SetActive(true);
                counter.value = 0;
                break;
                
        }
        
    }
}

